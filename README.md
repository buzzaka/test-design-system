# Engie Design System - Starter Kit 


## Requirements

| Package           | Check          | Informations |
| ----------------- | -------------- | ------------ |
| Node.js >= 8.0.0  | node --version | [nodejs.org](https://nodejs.org/en/download/) |
| npm >= 6.0.0      | npm  --version | [docs.npmjs.com](https://docs.npmjs.com)  |
| npx >= 9.0.0      | npx --version  | [npx](https://www.npmjs.com/package/npx)  |


## Install

### Clone this repository

```
git clone -b basic --single-branch https://bitbucket.org/buzzaka/test-design-system engie-design-system-basic
```

### Configuring access to the package

Follow this steps : https://design.digital.engie.com/design-system/getting-started/developers/


### Install dependencies

```
npm i @engie/design-system --save
npm install
```

### Load package

** Css**

Copy-paste the stylesheet `<link>` into your `<head>` before all other stylesheets to load our CSS.

```
<link href="/node_modules/@engie/design-system/lib/css/nj-components.min.css" rel="stylesheet">
```

** Js **

Place the following `<script>` near the end of your pages, right before the closing `</body>` tag, to enable them. 

```
<script src="/node_modules/@engie/design-system/lib/js/nj-components.js"></script>
```

### Start coding

From the terminal, 
```
npx gulp
```
