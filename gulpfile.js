(() => {

  'use strict';

  /**************** Gulp.js 4 configuration ****************/

  const

    // development or production
    devBuild  = ((process.env.NODE_ENV || 'development').trim().toLowerCase() === 'development'),

    // modules
    gulp          = require('gulp'),
    browsersync   = devBuild ? require('browser-sync').create() : null;


  console.log('Gulp', devBuild ? 'development' : 'production', 'build');

  /**************** server task (now private) ****************/

  const syncConfig = {
    server: {
      baseDir   : './',
      index     : 'index.html'
    },
    port        : 8000
  };

  // browser-sync
  function reload(done) {
    browsersync.reload();
    done();
  }

  function server(done) {
    if (browsersync) browsersync.init(syncConfig);
    done();
  }


  /**************** watch task ****************/

  const watch = () => { gulp.watch(['*.html', 'assets/css/**/*.css', 'assets/js/**/*.js'], reload) }

  /**************** default task ****************/

  exports.default = gulp.series( server, watch);

})();
